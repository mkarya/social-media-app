import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

import {FormGroup, FormControl} from '@angular/forms'

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css'] 
   
})
export class ForgetpasswordComponent implements OnInit {
 
  constructor() { }
 forgotpass;
  ngOnInit() {

    this.forgotpass = new FormGroup({
       emailorphone: new FormControl("")
    });
  }

}
