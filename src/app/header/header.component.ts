import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }
   form;
  ngOnInit() {

    this.form = new FormGroup({
      emailorphone: new FormControl(""),
      loginpassword: new FormControl("") 
    })

  }

  onSubmit = function(user){
    console.log(user)
  }

}
