import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignuphomeComponent } from './signuphome.component';

describe('SignuphomeComponent', () => {
  let component: SignuphomeComponent;
  let fixture: ComponentFixture<SignuphomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignuphomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignuphomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
