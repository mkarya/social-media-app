import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms'
@Component({
  selector: 'app-signuphome',
  templateUrl: './signuphome.component.html',
  styleUrls: ['./signuphome.component.css']
})
export class SignuphomeComponent implements OnInit {

  signupform;

  constructor() { }

  ngOnInit() {
    this.signupform = new FormGroup({
      firstname: new FormControl(""),
      surname: new FormControl(""),
      mobileemail: new FormControl(""),
      signuppassword: new FormControl(""),
      bdate: new FormControl(""),
      bmonth: new FormControl(""),
      byear: new FormControl(""),
      gender: new FormControl("")

    });
  }
default = "select";
  dates = [
     
     {value:1, label: 1},
    {value:2, label: 2},
    {value:3, label: 3},
    {value:4, label: 4},
    {value:5, label: 5},

  ]

}
