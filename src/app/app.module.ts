import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {  ReactiveFormsModule, FormsModule } from '@angular/forms'
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomecontentComponent } from './homecontent/homecontent.component';
import { SignuphomeComponent } from './signuphome/signuphome.component';
import { AboutpageComponent } from './aboutpage/aboutpage.component';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';


const myRoute: Routes = [ 
  
 
  { path: '', component: HomecontentComponent },
  {path: "about", component:AboutpageComponent},
  {path: "forgetpassword", component: ForgetpasswordComponent}
  
] 

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomecontentComponent,
    SignuphomeComponent,
    AboutpageComponent,
    ForgetpasswordComponent
  ],
  imports: [
    RouterModule.forRoot(myRoute),
    BrowserModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
