import { SocialappPage } from './app.po';

describe('socialapp App', () => {
  let page: SocialappPage;

  beforeEach(() => {
    page = new SocialappPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
